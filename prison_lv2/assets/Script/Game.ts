const {ccclass, property} = cc._decorator;
const t = cc.tween;
window.trial = 0;
@ccclass
export default class Game extends cc.Component {

    @property([cc.ParticleSystem])
    firework: cc.ParticleSystem[] = []

    @property(cc.Node)
    nextbg: cc.Node = null;

    @property(sp.Skeleton)
    lupin: sp.Skeleton = null;

    @property(sp.Skeleton)
    police: sp.Skeleton = null;

    @property(cc.Node)
    option1: cc.Node = null;
    
    @property(cc.Node)
    option2: cc.Node = null;
    
    @property(cc.Node)
    hand: cc.Node = null;

    @property(cc.Node)
    download: cc.Node = null;

    @property(cc.Node)
    shadow: cc.Node = null;

    @property(cc.Node)
    fail: cc.Node = null;

    @property(cc.Node)
    success: cc.Node = null;

    @property(sp.Skeleton)
    dog: sp.Skeleton = null;

    @property(cc.AudioClip)
    clickAudio: cc.AudioClip = null;

    @property(cc.AudioClip)
    failAudio: cc.AudioClip = null;

    @property(cc.AudioClip)
    winAudio: cc.AudioClip = null;

    @property(cc.AudioClip)
    screamAudio: cc.AudioClip = null;

    @property(cc.AudioClip)
    dingAudio: cc.AudioClip = null;

    @property(cc.AudioClip)
    crawlAudio: cc.AudioClip = null;

    @property(cc.AudioClip)
    hitAudio: cc.AudioClip = null;

    @property(cc.AudioClip)
    swingAudio: cc.AudioClip = null;

    @property(cc.Node)
    mainCamera: cc.Node = null;

    @property(cc.Node)
    camera2D: cc.Node = null;

    isClicked: boolean = false

    lupinStartPos = cc.v3(-312.295, -509, 0)
    dogStartPos = cc.v3(947, -559, 0)

    countWinAnim = 0
    crawlAudioId = -1
    currentCanvasSize: cc.Size = null
    currentPlayingLupin = ""
    onLoad() {
        this.lupin.setAnimation(0, "general/stand_thinking", true)
        this.lupin.setAnimation(1,"emotion/idle",true)

        this.dog.setAnimation(0, "run", true)

        var canvasSize = cc.view.getCanvasSize();
        this.currentCanvasSize = canvasSize;
        if (window.mintegral){
            this.download.active = true
        }
        this.updateOrientation()

        t(this.dog.node)
            .to(2, {position: cc.v3(825, -523)})
            .call(() => {
                this.lupin.setAnimation(0, "general/back", false)
                this.lupin.setAnimation(1,"emotion/worry",true)
                this.dog.setAnimation(0, "stand_angry", true)
            })
            .start()
    }
    updateOrientation(){
        var optionNode = this.node.getChildByName("Options")
        const container = this.node.getChildByName("Container")
        if (this.currentCanvasSize.width > this.currentCanvasSize.height) {
            cc.Canvas.instance.fitHeight = false
            cc.Canvas.instance.fitWidth = true
            optionNode.position = cc.v3(700,0,0)
            this.option1.position = cc.v3(100,200,0)
            this.option2.position = cc.v3(100,-200,0)
            this.download.position = cc.v3(800,-500,0)
            
            container.position = cc.v3(-1140, 0, 0)
            this.mainCamera.active = true
            this.camera2D.active = false

        } else {
            cc.Canvas.instance.fitHeight = true
            cc.Canvas.instance.fitWidth = false
            optionNode.position = cc.v3(0,-700)
            this.option1.position = cc.v3(-200,0)
            this.option2.position = cc.v3(200,0)
            this.download.position = cc.v3(0,-1000)

            container.position = cc.v3(-642, 0, 0)
            this.mainCamera.active = false
            this.camera2D.active = true
        }

    }
    showOptions(focus = false){
        // init logic

        var option = this.node.getChildByName("Options")
        option.scale = 0
        t(option)
            .delay(1)
            .to(0.3, {scale: 1})
            .call(()=>{
                cc.audioEngine.play(this.dingAudio, false, 1)
            })
            .delay(1)
            .call(()=>{
                this.hand.active = true
                this.startHandAnim(focus)
            })
            .start()
    }

    startHandAnim(focus = false){
        var pos1 = cc.v3();
        var pos2 = cc.v3();
        var handPos = cc.v3()
        
        if (this.currentCanvasSize.width > this.currentCanvasSize.height) {
            pos1 = cc.v3(150, 100)
            pos2 = cc.v3(150, -300)
            handPos = cc.v3(150,-300)
        } else {
            pos1 = cc.v3(-170, -100)
            pos2 = cc.v3(230, -100)
            handPos = cc.v3(230,-100)
        }
        var vibration = t().by(0.1, {scale: -0.2}).by(0.1, {scale: 0.2});
        if (focus) {
            t(this.hand)
            .to(0.1, {opacity: 255})
            .to(0.3, {position: pos1})
            .delay(0.5)
            .tag(100)
            .repeatForever(t()
                            .to(0.6,{position:pos1})
                            .call(()=>{
                                vibration.clone(this.option1).start()
                            })
                            .then(
                                vibration.clone(this.hand)
                            )                           
                        )
            .start()
        } else {
            t(this.hand)
            .to(0.1, {opacity: 255})
            .to(0.3, {position: handPos})
            .delay(0.5)
            .tag(100)
            .repeatForever(t()
                            .to(0.6,{position:pos1})
                            .call(()=>{
                                vibration.clone(this.option1).start()
                            })
                            .then(
                                vibration.clone(this.hand)
                            )
                            .to(0.6,{position:pos2})
                            .call(()=>{
                                vibration.clone(this.option2).start()
                            })
                            .then(
                                vibration.clone(this.hand)
                            )                               
                        )
            .start()
        }

    }

    start () {
        this.showOptions()
        window.gameReady && window.gameReady();
    }
    downloadClicked(){
        window.openStore()
    }
    selectOption1(){
        cc.audioEngine.play(this.clickAudio, false, 1)
        if (this.hand.active){
            if (this.isClicked == false) {
                this.isClicked = true
                var selected = this.option1.getChildByName("selected")
                selected.active = true
                this.hand.active = false
                cc.Tween.stopAllByTag(100)

                this.fadeScene(()=>{
                },
                this.runOp1.bind(this))
            }
        }
        if (window.trial == 1) {
            window.openStore()
        }
    }
    fadeScene(prepareFunc, callFunc){
        this.shadow.active = true

        t(this.shadow)
            .to(0.5, {opacity: 255})
            .call(prepareFunc)
            .delay(0.5)
            .to(0.5, {opacity: 0})
            .call(() => {
                this.shadow.active = false
                callFunc()
            })
            .start()
    }
    runOp1(){
        this.lupin.setCompleteListener(trackEntry => {
            if (trackEntry.animation.name == "level_2/lv2_stg1_bone") {
                this.dog.setAnimation(0, "run_1", true);
                this.lupin.addAnimation(1, "emotion/whistle", true);
                this.lupin.addAnimation(0, "emotion/idle", true);
                t(this.dog.node).to(1.5, {position: cc.v3(-182, -523)}).start();
                t(this.node)
                    .delay(1.5)
                    .call(() => {
                        this.lupin.setAnimation(0, "general/walk", true);
                        t(this.camera2D).to(4, {position: cc.v3(990, 0)}).start();
                        t(this.lupin.node)
                            .to(4, {position: cc.v3(1730, -500)})
                            .call(() => {
                                cc.audioEngine.play(this.winAudio, false, 1)
                                this.lupin.timeScale = 0
                                var tick = this.option1.getChildByName("tick")
                                tick.scale = 0
                                tick.active = true
                                t(tick).to(0.5,{scale: 1}).start()
                                this.shadow.opacity = 150
                                this.shadow.active = true
                                this.success.active = true
                                t(this.success)
                                    // first reset node properties
                                    .set({ opacity: 0, scale: 10})
                                    // parallel exec tween
                                    .parallel(
                                        t().to(1, { opacity: 255, scale: 1, y: 0 }, { easing: 'quintInOut' }),
                                        t().to(1.5, { y: this.success.y }, { easing: 'backOut' })
                                            .call(()=>{
                                                for(let f of this.firework) {
                                                    this.scheduleOnce(()=>{
                                                        f.node.active = true
                                                    }, Math.random())
                                                }
                                            })
                                            .delay(1)
                                            .call(()=>{
                                                this.fadeScene(()=>{
                                                    this.option1.getChildByName("tick").active = false
                                                    this.option1.getChildByName("selected").active = false
                                                    this.success.active = false
                                                    this.dog.node.active = false
                                                    this.pushNextScene()

                                                }, ()=>{
                                                    window.trial = 1

                                                })
                                            })
                                    ).start()
                            })
                            .start();
                    })
                .start();
            }
        })

        t(this.node)
            .delay(1)
            .call(() => {
                this.lupin.setAnimation(0, "level_2/lv2_stg1_bone", false);
                this.lupin.setAnimation(1, "emotion/sinister", false);
                
            })
            .start();
    }
    runOp2(){
        this.lupin.setCompleteListener(trackEntry => {
            if (trackEntry.animation.name == "level_2/lv2_stg1_potato") {
                this.lupin.setAnimation(1, "emotion/fear_2", true);
                this.lupin.setAnimation(0, "general/stand_nervous", true);
                this.dog.setAnimation(0, "shout", true);
                t(this.police.node).delay(2)
                    .call(() => {
                        this.police.setAnimation(0, "police/general/run", true);
                        t(this.police.node)
                            .to(1, {position: cc.v3(1000, -510)})
                            .call(() => {
                                this.police.setAnimation(
                                    0,
                                    "police/general/gun_raise",
                                    false,
                                );
                            })
                            .start();
                    })
                    .start();
            }
        })

        this.police.setCompleteListener(trackEntry => {
            if (trackEntry.animation.name == "police/general/gun_raise") {
                var tick = this.option2.getChildByName("tick")
                tick.scale = 0
                tick.active = true
                t(tick).to(0.5,{scale: 1}).start()
                this.shadow.opacity = 150
                this.shadow.active = true
                this.fail.active = true
                cc.audioEngine.play(this.failAudio, false, 1)
                t(this.fail)
                    // first reset node properties
                    .set({ opacity: 0, scale: 10})
                    // parallel exec tween
                    .parallel(
                    t().to(1, { opacity: 255, scale: 1, y: 0 }, { easing: 'quintInOut' }),
                    t().to(1.5, { y: this.fail.y }, { easing: 'backOut' })
                    )
                    .delay(1)
                    .call(()=>{
                        this.fadeScene(()=>{
                            this.option2.getChildByName("tick").active = false
                            this.option2.getChildByName("selected").active = false
                            this.fail.active = false
                            this.resetGame()
                        },()=>{
                            window.trial = 1
                        })
                    })
                    .start()
            }
        });

        t(this.node).delay(1).call(() => {
            this.lupin.setAnimation(0, "level_2/lv2_stg1_potato", false);
        }).start();
    }
    selectOption2(){
        cc.audioEngine.play(this.clickAudio, false, 1)
        if (this.hand.active){
            if (this.isClicked == false) {
                this.isClicked = true
                var selected = this.option2.getChildByName("selected")
                selected.active = true
                this.hand.active = false
                cc.Tween.stopAllByTag(100)
                this.fadeScene(()=>{
                    cc.log("option2", "fade")
                }, this.runOp2.bind(this))
            }
        }
        if (window.trial == 1) {
            window.openStore()
        }
    }
    resetGame(){
        window.gameEnd && window.gameEnd()
        this.lupin.setAnimation(0,"level_8/back", false)
        this.lupin.setAnimation(1, "emotion/worry", true)

        this.dog.setAnimation(0, "stand_angry", true)

        this.police.node.position = cc.v3(2519, -510)
        this.showOptions(true)
    }
    pushNextScene(){
        window.gameEnd && window.gameEnd()

        const container = this.node.getChildByName('Container')

        var bg = container.getChildByName("background")
        var nextbg = container.getChildByName("nextbg")
        bg.active = false
        nextbg.active = true

        container.getChildByName('nexttower1').active = true
        container.getChildByName('nexttower2').active = true

        this.camera2D.position = cc.v3(0, 0)
        this.lupin.node.position = cc.v3(302, -445)
        this.lupin.timeScale = 1
        this.lupin.setAnimation(0,"emotion/nervous", true)
        this.lupin.setAnimation(1, "general/walk_slow", true)
        this.option1.getChildByName("-").active = false
        this.option1.getChildByName("--").active = true
        this.option2.getChildByName("-").active = false
        this.option2.getChildByName("--").active = true

        t(this.lupin.node)
            .to(2.5, {position: cc.v3(500, -445)})
            .call(() => {
                this.lupin.setAnimation(1, "level_3/look_down", false);
                this.lupin.setMix("level_3/look_down_loop", "emotion/idle", 0.3);
                this.lupin.addAnimation(1, "level_3/look_down_loop", false);
                this.lupin.setAnimation(0, "emotion/fear_1", false);
                this.lupin.setCompleteListener(trackEntry => {
                    if (trackEntry.animation.name == "level_3/look_down_loop")
                    {
                        this.lupin.addAnimation(1, "emotion/idle", true);
                        this.lupin.setAnimation(0, "emotion/fear_2", false);
                        this.lupin.setCompleteListener(trackEntry => {
                            if (trackEntry.animation.name == "emotion/fear_2")
                            {
                                if (count < 1)
                                {
                                    this.showOptions()
                                    ++count;
                                }
                            }
                        });
                    }
                })
            })
            .start();

    }
    update(_dt){
        var canvasSize = cc.view.getCanvasSize();
        if (canvasSize.equals(this.currentCanvasSize) == false){
            cc.log("update canvas: ", canvasSize)
            this.currentCanvasSize = canvasSize
            this.updateOrientation()
            if (this.node.getChildByName("Options").active){
                cc.Tween.stopAllByTag(100)
                this.startHandAnim()
            }
        }
    }
}
